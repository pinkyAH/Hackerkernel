Use this font  ( Comfortaa )

Prepare the UI same as (UI Reference image: 00 Login.jpg), make sure that UI mobile responsive and same as Image: 00 Login.jpg
Use below API for login after successfully login navigate the user to a ‘home page’ make sure to provide logout option in home page
 
Login Page - Functionality
 
Login - User would entre Email & Password to login
Email
Password
Login Button 
 
 
 
URL:  https://reqres.in/api/login
METHOD: POST
BODY: 
 
{
    "email": "eve.holt@reqres.in",
    "password": "pistol"
}
 
BODY TYPE: JSON
 
 
 
 
 
 
Postman Response SnapShot

 
 


Design of Home page should be same as the sample UI provided (UI Reference: 02 Dashboard - 01 Pay.jpg)
Home Page should be fully responsive with Sticky Header & Collapsible Sidebar
Use localstorage to save user data which you receive at the time of login
Use routing for login, register and home page
After successfully login the user will not be able to go back or open login and register page
After logout navigate the user to the login page and now the user will not be able to open the home page & Delete user data saved in localstorage.
 

Hackerkernel
